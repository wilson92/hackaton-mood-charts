import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";
import "./index.css";

// const LINES = {
//   "neutral",
//   "happy",
//   "sad",
//   "angry",
//   "fearful",
//   "disgusted",
//   "surprised",
//   "meetingId",
//   "participantId",
// };

const LINES = [
  { name: "neutral", dataKey: "neutral", fill: "#8884d8" },
  { name: "happy", dataKey: "happy", fill: "#82ca9d" },
  { name: "sad", dataKey: "sad", fill: "#8884d8" },
  { name: "angry", dataKey: "angry", fill: "#82ca9d" },
  { name: "fearful", dataKey: "fearful", fill: "#8884d8" },
  { name: "disgusted", dataKey: "disgusted", fill: "#82ca9d" },
  { name: "surprised", dataKey: "surprised", fill: "#8884d8" },
];

const fakeData = [
  {
    name:"Wilson 1",
    neutral: 1,
    happy: 2,
    sad: 1,
    angry: 2,
    fearful: 3,
    disgusted: 1,
    surprised: 3,
  },
  {
    name:"Wilson 2",
    neutral: 0.9991528987884521,
    happy: 0.000024047796614468098,
    sad: 0.0007439805776812136,
    angry: 0.00007823136547813192,
    fearful: 3.1238442943504197,
    disgusted: 1.3385985653258103,
    surprised: 3.6761929322892684,
  },
  {
    name:"Wilson 3",
    neutral: 0.9991528987884521,
    happy: 0.000024047796614468098,
    sad: 0.0007439805776812136,
    angry: 0.00007823136547813192,
    fearful: 3.1238442943504197,
    disgusted: 1.3385985653258103,
    surprised: 3.6761929322892684,
  },
  {
    name:"Wilson 4",
    neutral: 0.9991528987884521,
    happy: 0.000024047796614468098,
    sad: 0.0007439805776812136,
    angry: 0.00007823136547813192,
    fearful: 3.1238442943504197,
    disgusted: 1.3385985653258103,
    surprised: 3.6761929322892684,
  },
];

const data = [
  {
    name: "Page A",
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: "Page B",
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: "Page C",
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: "Page D",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "Page E",
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: "Page F",
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: "Page G",
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
];

export default function App() {
  return (
    <BarChart
      width={1000}
      height={300}
      data={fakeData}
      margin={{
        top: 5,
        right: 30,
        left: 20,
        bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      {/* <Bar dataKey="neutral" fill="#8884d8" /> */}
      {LINES.map((line) => {
        console.log(line);
        return <Bar {...line}  />;
      })}
    </BarChart>
  );
}
